import { Application } from 'express';

export interface IProcessEnv {
  ACCESS_LOG_PATH: string;
  CONSULTA_CEP_URL: string;
  ALGORITHM: string;
  LOG_PATH: string;
  MYSQL_DB: string;
  MYSQL_PASSWORD: string;
  MYSQL_PORT?: number;
  MYSQL_URL: string;
  MYSQL_USER: string;
  NODE_ENV?: string;
  PORT: number;
  REDIS_URL: string;
  SECRET_KEY: string;
  SERVER_URL: string;
}

export interface IConfig extends IProcessEnv {
  app: Application;
  isLocal?: boolean;
  ENV?: string;
}