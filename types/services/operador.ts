import { IResponse } from './response';

export interface IOperadorService {
  login: (usuario: string, senha: string) => Promise<IResponse>;
};
