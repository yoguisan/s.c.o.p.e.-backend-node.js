export interface IResponse {
  status: number;
  error?: any;
}