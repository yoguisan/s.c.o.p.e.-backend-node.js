import IEndereco from './endereco';
import ILogin from './login';

interface IOperador extends ILogin, IEndereco {
  nome: string;
  verificado?: boolean;
  premium?: boolean;
}

export default IOperador;
