interface IEndereco {
  cep: string;
  numero: number;
  cidade: string;
  estado: string;
  bairro: string;
}

export default IEndereco;
