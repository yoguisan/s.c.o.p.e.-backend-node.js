export interface IAssembleParams {
  nome: string;
  usuario: string,
  senha: string,
  verificado?: boolean,
  premium?: boolean,
  cep: string,
  endereco: string,
  numero: string,
  cidade: string,
  estado: string,
  bairro: string,
};