module.exports = {
  collectCoverage: true,
  coverageDirectory: './coverage/',
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 10,
    },
  },
  // setupFiles: ['mock-local-storage'],
  setupFilesAfterEnv: ['./setup-jest.js'],
  verbose: true,
  testResultsProcessor: 'jest-sonar-reporter',
};
