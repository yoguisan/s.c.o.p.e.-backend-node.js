FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm i

COPY . .

ARG PORT
ARG ENV
ARG REDIS_URL
ARG MYSQL_URL
ARG MYSQL_USER
ARG MYSQL_PASSWORD
ARG MYSQL_DB
ARG CONSULTA_CEP_URL
ARG ALGORITHM
ARG SECRET_KEY

ENV PORT $PORT
ENV ENV $ENV
ENV REDIS_URL $REDIS_URL
ENV MYSQL_URL $MYSQL_URL
ENV MYSQL_USER $MYSQL_USER
ENV MYSQL_PASSWORD $MYSQL_PASSWORD
ENV MYSQL_DB $MYSQL_DB
ENV CONSULTA_CEP_URL $CONSULTA_CEP_URL
ENV ALGORITHM $ALGORITHM
ENV SECRET_KEY $SECRET_KEY

EXPOSE 8080

CMD ["npm", "run", "dev"]

# RUN npm start