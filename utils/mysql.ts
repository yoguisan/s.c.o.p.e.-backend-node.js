import * as mysql from 'mysql2/promise';

import Config from '../config';

const {
  MYSQL_URL: mysqlUrl,
  MYSQL_USER: mysqlUser,
  MYSQL_PASSWORD: mysqlPassword,
  MYSQL_DB: mysqlDb,
  MYSQL_PORT: mysqlPort,
  ENV,
} = Config;

export const connect = async (url, user, password, db, port = 3306) => {
  if (global.connection && global.connection.state !== 'disconnected') {
    console.log('Conectado ao MySQL');
    return global.connection;
  }

  const address = `mysql://${user}:${password}@${url}:${port}/${db}`;

  const connection = await mysql.createConnection(address);
  switch (ENV.toLowerCase()) {
    case 'local':
    case 'dev':
      console.log(`Usuário ${user} conectado no banco ${db}`);
      break;
    default:
      break;
  }
  global.connection = connection;
  return connection;
};

export const MySQL = async () => connect(
  mysqlUrl,
  mysqlUser,
  mysqlPassword,
  mysqlDb,
  mysqlPort,
);

const isLocal = ENV.toLowerCase() === 'local';
 
export const select = async (table, paramsToSelect, whereParams) => {
  const conn = await MySQL();
  const where = [];
  if (whereParams) {
    Object.keys(whereParams).forEach((param) => where.push(`${param}="${whereParams[param]}"`));
  }
  const query = `
    SELECT ${paramsToSelect ? paramsToSelect.join(', ') : '*'}
    FROM ${table}
    ${whereParams ? `WHERE ${where.join(' AND ')}` : ''}
  ;`;
  if (isLocal) {
    console.log(query);
  }
  const [rows] = await conn.query(query);
  return rows;
};

export const insert = async (table: string, paramsToInsert: any, isAutoIncrement?: boolean) => {
  const conn = await MySQL();
  const query = `
    INSERT INTO ${table}
    VALUES (
      ${isAutoIncrement ? '0, ' : ''}
      ${paramsToInsert.join(',')}
    )
  ;`;
  if (isLocal) {
    console.log(query);
  }
  const result = await conn.query(query);
  return result;
};

export const update = async (table: string, params, whereParam?: string, whereId?: string) => {
  const conn = await MySQL();
  const setParams = [];
  Object.keys(params)
    .forEach((key) => setParams.push(`${key}=${params[key]}`));
  const query = `
    UPDATE ${table}
    SET ${setParams}
    WHERE ${whereParam}=${whereId}
  `;
  if (isLocal) {
    console.log(query);
  }
  const result = await conn.query(query);
  return result;
};
