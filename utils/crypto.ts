import * as crypto from 'crypto';

import CryptoConfig from './crypto-config';

const {
  ALGORITHM: algorithm,
  SECRET_KEY: secretKey,
} = CryptoConfig;

const iv = crypto.randomBytes(16);

export const hashString = (string) => {
  const md5sum = crypto.createHash('sha512');

  return md5sum.update(string).digest('hex');
};

export const hashPassword = hashString;

export const encrypt = (text) => {
  const cipher = crypto.createCipheriv(
    algorithm,
    secretKey,
    iv,
  );

  const encrypted = Buffer.concat([cipher.update(
    typeof (text) === 'object'
      ? JSON.stringify(text)
      : text,
  ), cipher.final()]);

  return {
    iv: iv.toString('hex'),
    content: encrypted.toString('hex'),
  };
};

export const decrypt = (hash) => {
  const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'));

  const decrypted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]).toString();

  let isJson = true;

  try {
    JSON.parse(decrypted);
  } catch (error) {
    isJson = false;
  }

  return isJson ? JSON.parse(decrypted) : decrypted.toString();
};
