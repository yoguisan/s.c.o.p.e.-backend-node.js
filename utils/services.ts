import fetch from 'node-fetch';
import logger from './logger';

import Config from '../config';

const {
  ENV,
} = Config;

const headers = {};

const isLocal = ENV === 'local';
 
export const request = async (url, data, options) => fetch(url, {
  ...options,
  body: JSON.stringify({ ...data }),
})
  .then((res) => res.json())
  .then((dt) => dt)
  .catch((error) => {
    logger.error(error);
    throw new Error(error);
  });

export const get = async (url, data?) => {
  const queryParams = [];
  if (data) {
    Object.keys(data).forEach((key) => queryParams.push(`${key}=${data[key]}`));
  }

  if (isLocal) {
    logger.info(`GET para URL: \n${url}`);
  }

  return fetch(`${url}${
    queryParams.length > 0
      ? `?${queryParams.join('&')}`
      : ''
  }`, {
    headers,
  })
    .then((res) => res.json())
    .then((dt) => dt)
    .catch((error) => error);
};

export const post = async (url, data) => request(url, data, {
  method: 'POST',
  headers,
});

export const put = async (url, data) => request(url, data, {
  method: 'PUT',
  headers,
});

