import Morgan from 'morgan';
import * as fs from 'fs';

const {
  NODE_ENV: ENV,
  ACCESS_LOG_PATH = 'logs',
} = process.env;

export const isLocal = ENV === 'local';

const format = isLocal ? 'dev' : 'combined';

const stream = fs.createWriteStream(`./${ACCESS_LOG_PATH}/access.log`, { flags: 'a' });

export default Morgan(format, { stream });
export { ACCESS_LOG_PATH };
