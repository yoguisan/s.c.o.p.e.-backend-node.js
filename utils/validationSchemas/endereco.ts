const cep = {
  cep: {
    in: ['body'],
    isNumeric: true,
    isLength: {
      min: 8,
      max: 8,
    },
    errorMessage: 'Favor informar o CEP',
  },
};

const endereco = {
  ...cep,
  endereco: {
    in: ['body'],
    isAlphanumeric: true,
    isLength: {
      min: 3,
      max: 128,
    },
    errorMessage: 'Favor informar o endereco',
  },
  numero: {
    in: ['body'],
    isNumeric: true,
    errorMessage: 'Favor informar o número',
  },
  cidade: {
    in: ['body'],
    isAlphanumeric: true,
    isLength: {
      min: 2,
      max: 30,
    },
    errorMessage: 'Favor informar a cidade',
  },
  estado: {
    in: ['body'],
    isAlpha: true,
    isLength: {
      min: 2,
      max: 2,
    },
    errorMessage: 'Favor informar o estado',
  },
  bairro: {
    in: ['body'],
    isAlphanumeric: true,
    isLength: {
      min: 2,
      max: 128,
    },
  },
};

export default {
  cep,
  endereco,
};
