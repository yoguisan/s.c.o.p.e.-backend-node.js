import endereco from './endereco';
import login from './login';

const signup = {
  ...login,
  ...endereco,
  nome: {
    in: ['body'],
    isAlphanumeric: true,
    isLength: {
      min: 2,
      max: 128,
    },
    errorMessage: 'Favor informar o nome do campo',
  },
  idDono: {
    in: ['body'],
    isNumeric: true,
    errorMessage: 'ID do dono do campo não informado',
  },
};

export default {
  login,
  endereco,
  signup,
  update: {
    ...signup,
    idCampo: {
      in: ['body'],
      isNumeric: true,
      errorMessage: 'ID do campo não informado',
    },
  },
};
