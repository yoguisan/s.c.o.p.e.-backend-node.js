export default {
  cadastro: {
    idCampo: {
      in: ['body'],
      isNumeric: true,
      errorMessage: 'ID do campo não informado',
    },
    nome: {
      in: ['body'],
      isAlphanumeric: true,
      isLength: {
        min: 1,
        max: 128,
      },
      errorMessage: 'Favor informar o nome do produto',
    },
    categoria: {
      in: ['body'],
      isAlphanumeric: true,
      isLength: {
        min: 1,
        max: 32,
      },
      errorMessage: 'Favor informar a categoria do produto',
    },
    valor: {
      in: ['body'],
      isNumeric: true,
      errorMessage: 'Favor informar o valor do produto',
    },
  },
};
