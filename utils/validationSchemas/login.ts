export default {
  login: {
    usuario: {
      in: ['body'],
      errorMessage: 'Favor informar um e-mail válido',
      isLength: {
        max: 128,
      },
      isEmail: true,
    },
    senha: {
      in: ['body'],
      isLength: {
        min: 128,
        max: 128,
      },
      errorMessage: 'Senha é requerida',
    },
  },
};
