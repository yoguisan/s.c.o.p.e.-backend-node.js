import endereco from './endereco';
import login from './login';

const signup = {
  ...login,
  ...endereco,
};

export default {
  endereco,
  login,
  signup,
  update: signup,
};
