const proc = process.env;

const {
  ALGORITHM,
  SECRET_KEY,
} = proc;

export default {
  ALGORITHM,
  SECRET_KEY,
};
