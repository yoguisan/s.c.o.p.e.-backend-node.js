const apiPath = `${__dirname.replace('utils\\swagger', '')}/controllers`;

console.log(apiPath)

const { SERVER_URL } = process.env;

module.exports = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'S.C.O.P.E. (Node.js backend)',
      version: '0.1.0',
      description: 'Backend em Node.js para o sistema S.C.O.P.E.',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'Nelson Salles de Barros Filho',
        url: 'https://nelsonsbf.tk',
        email: 'nelsonsbf@protonmail.com',
      },
    },
    servers: [{
      // url: 'http://localhost:3000/books',
      url: `${SERVER_URL}/campo`,
    }],
  },
  apis: [
    `${apiPath}/campo.js`,
    `${apiPath}/endereco.js`,
    `${apiPath}/operador.js`,
    `${apiPath}/produto.js`,
  ],
};

module.exports.SERVER_URL = SERVER_URL;
