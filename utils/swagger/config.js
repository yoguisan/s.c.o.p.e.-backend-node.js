const swaggerJsdoc = require('swagger-jsdoc');

const options = require('./options');

const specs = swaggerJsdoc(options);

module.exports = specs;
