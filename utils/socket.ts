import * as http from 'http';
import { Server } from 'socket.io';

export default (app) => {
  const server = http.createServer(app);
  const io = new Server(server);

  return io;
};
