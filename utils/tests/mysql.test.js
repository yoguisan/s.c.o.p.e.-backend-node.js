const MySQL = require('../mysql');

const { select } = MySQL;

const query = select(
  'tabela',
  [
    'parametro1',
    'parametro2',
    'parametro3',
  ],
  {
    parametro1: 'um',
    parametro2: 'dois',
    parametro3: 'três',
  },
);

describe('testes de criação de query\'s', () => {
  test('cria uma query de SELECT com os parâmetros informados', () => {
    expect(query).toBeDefined();
  });
});

describe('testes de conexão com o banco de dados', () => {
  test('retorna uma conexão ativa', async () => {
    await expect(MySQL()).toBeDefined();
  });

  test('executa um SELECT', async () => {
    await expect(MySQL().query(query)).toBeDefined();
  });
});
