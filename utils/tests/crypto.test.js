const {
  hashPassword,
  encrypt,
  decrypt,
} = require('../crypto');

const password = 'password';
const hash = 'B109F3BBBC244EB82441917ED06D618B9008DD09B3BEFD1B5E07394C706A8BB980B1D7785E5976EC049B46DF5F1326AF5A2EA6D103FD07C95385FFAB0CACBC86';

const string = 'Me leia se puder';
const obj = {
  nome: 'Lango',
  idade: 51,
  email: 'lango@lango.com',
};

describe('testes de senhas', () => {
  test('gera o hash de uma senha', () => {
    expect(hashPassword(password)).toEqual(hash.toLowerCase());
  });
});

describe('testes de criptografia', () => {
  const encryptedStr = encrypt(string);
  const encryptedObj = encrypt(obj);

  test('encripta uma string', () => {
    expect(encryptedStr).toBeDefined();
    expect(typeof (encryptedStr)).toEqual('object');
  });
  test('encripta um objeto', () => {
    expect(encryptedObj).toBeDefined();
    expect(typeof (encryptedObj)).toEqual('object');
  });

  test('decripta uma string', () => {
    const decrypted = decrypt(encryptedStr);

    expect(decrypted).toEqual(string);
  });
  test('decripta um objeto', () => {
    const decrypted = decrypt(encryptedObj);
    expect(decrypted).toEqual(obj);
  });
});
