import { IProcessEnv } from '@customTypes/config';

declare global {
  namespace NodeJS {
    interface ProcessEnv extends IProcessEnv {
    }
  }
}

export {};