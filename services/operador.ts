import { select, insert, update as MySQLUpdate } from '@utils/mysql';
import Config from '../config';
import logger from '@utils/logger';
import { IOperadorService } from '@customTypes/services/operador';
import { IResponse } from '@customTypes/services/response';

const {
  isLocal,
} = Config;

const assembleParams = (params, isUpdate?: boolean) => {
  const {
    nome,
    usuario,
    senha,
    verificado = 0,
    premium = 0,
    /*
    cep,
    endereco,
    numero,
    cidade,
    estado,
    bairro,
    */
  } = params;

  const assembledParams = isUpdate
    ? {
      nome: `"${nome}"`,
      usuario: `"${usuario}"`,
      senha: `"${senha}"`,
      verificado: !!verificado,
      premium: !!premium,
      /*
      cep,
      endereco: `"${endereco}"`,
      numero,
      cidade: `"${cidade}"`,
      estado: `"${estado}"`,
      bairro: `"${bairro}"`,
      */
    }
    : [
      `"${nome}"`,
      `"${usuario}"`,
      `"${senha}"`,
      !!verificado,
      !!premium,
      /*
      cep,
      `"${endereco}"`,
      numero,
      `"${cidade}"`,
      `"${estado}"`,
      `"${bairro}"`,
      */
    ];

  return assembledParams;
};

export const login: IOperadorService['login'] = async (usuario, senha): Promise<IResponse> => {
  try {
    const dadosOperador = await select(
      'operador',
      [
        'idOperador',
        'nome',
        'usuario',
        'verificado',
        'premium',
      ],
      {
        usuario,
        senha,
      },
    );
    return dadosOperador[0]
      ? {
        status: 200,
        ...dadosOperador[0],
      } : {
        status: 201,
        message: 'Operador não cadastrado',
      };
  } catch (error) {
    logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};

export const signup = async (params) => {
  try {
    if (isLocal) {
      logger.info('CADASTRO DE OPERADOR:');
      logger.info(
        typeof params === 'object'
          ? JSON.stringify(params)
          : params,
      );
    }
    const paramsToInsert = assembleParams(params);
    await insert(
      'operador',
      paramsToInsert,
      true,
    );
    return {
      status: 201,
      message: 'Operador cadastrado com sucesso',
    };
  } catch (error) {
    logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};

export const update = async (params) => {
  try {
    const paramsToUpdate = assembleParams(params, true);
    const result = await MySQLUpdate(
      'operador',
      paramsToUpdate,
      'idOperador',
      params.idOperador,
    );
    return result;
  } catch (error) {
    logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};

export const buscarDados = async (idOperador, params) => {
  try {
    const dadosOperador = await select(
      'operador',
      params.length > 0
        ? params
        : '*',
      {
        idOperador,
      },
    );
    return dadosOperador[0]
      ? {
        status: 200,
        ...dadosOperador[0],
      } : {
        status: 201,
        message: 'Operador não cadastrado',
      };
  } catch (error) {
    logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};

