import * as MySQL from '@utils/mysql';
import Config from '../config';
import Logger from '@utils/logger';

const { ENV } = Config;

const {
  select,
  insert,
  update: MySQLUpdate,
} = MySQL;

const isLocal = ENV === 'local';

const assembleParams = (params, isUpdate?: boolean) => {
  const {
    nome,
    usuario,
    senha,
    cep,
    endereco,
    numero,
    cidade,
    estado,
    bairro,
    idDono,
    verificado = 0,
    premium = 0,
  } = params;

  const assembledParams = isUpdate
    ? {
      nome: `"${nome}"`,
      usuario: `"${usuario}"`,
      senha: `"${senha}"`,
      cep,
      endereco: `"${endereco}"`,
      numero,
      cidade: `"${cidade}"`,
      estado: `"${estado}"`,
      bairro: `"${bairro}"`,
      idDono,
      verificado: !!verificado,
      premium: !!premium,
    }
    : [
      `"${nome}"`,
      `"${usuario}"`,
      `"${senha}"`,
      cep,
      `"${endereco}"`,
      numero,
      `"${cidade}"`,
      `"${estado}"`,
      `"${bairro}"`,
      idDono,
      !!verificado,
      !!premium,
    ];

  return assembledParams;
};


export const login = async (usuario, senha) => {
    try {
      const dadosCampo = await select(
        'campo',
        [
          'idCampo',
          'nome',
          'usuario',
          'endereco',
          'idDono',
        ],
        {
          usuario,
          senha,
        },
      );
      return dadosCampo[0] || {
        status: 201,
        message: 'Campo não cadastrado',
      };
    } catch (error) {
      Logger.error(error);
      return {
        status: 400,
        error,
      };
    }
};

export const signup = async (params) => {
    try {
      if (isLocal) {
        Logger.info('PARÂMETROS RECEBIDOS PARA CADASTRAR CAMPO:');
        Logger.info(params);
      }
      const paramsToInsert = assembleParams(params);

      const result = await insert(
        'campo',
        paramsToInsert,
        true,
      );
      return {
        status: 201,
        data: result,
      };
    } catch (error) {
      Logger.error(error);
      return {
        status: 400,
        error,
      };
    }
};

export const update = async (params) => {
  try {
    const paramsToUpdate = assembleParams(params, true);

    const result = await MySQLUpdate(
      'campo',
      paramsToUpdate,
      'idCampo',
      params.idCampo,
    );
    return {
      status: 201,
      data: result,
    };
  } catch (error) {
    Logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};
export const buscarDados = async (idCampo, params) => {
    try {
      const dadosCampo = await select(
        'campo',
        params.length > 0
          ? params
          : '*',
        {
          idCampo,
        },
      );
      return dadosCampo[0] || {
        status: 201,
        message: 'Campo não cadastrado',
      };
    } catch (error) {
      Logger.error(error);
      return {
        status: 400,
        error,
      };
    }
};
