import Config from '../config';
import logger from '@utils/logger';
import { insert, update, select } from '@utils/mysql';

const {
  isLocal,
} = Config;

const assembleParams = (params, isUpdate?) => {
  const {
    id,
    nome,
    categoria,
    valor,
    idCampo,
  } = params;

  const assembledParams = isUpdate
    ? {
      id,
      nome: `"${nome}"`,
      valor,
      categoria: `"${categoria}"`,
      idCampo,
    }
    : [
      `"${nome}"`,
      `${valor}`,
      `"${categoria}"`,
      idCampo,
    ];

  return assembledParams;
};

export const listar = async (idCampo, paramsToSelect) => {
  try {
    const lista = await select(
      'produto',
      paramsToSelect,
      {
        idCampo,
      },
    );
    return {
      status: 200,
      lista,
    };
  } catch (error) {
    logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};

export const buscar = async (
  parametroBusca,
  valorBusca,
  precisaSerExato,
) => {};

export const cadastrar = async (params) => {
  const paramsToInsert = assembleParams(params);
  try {
    if (isLocal) {
      logger.info('CADASTRO DE PRODUTO:');
      logger.info(
        typeof params === 'object'
          ? JSON.stringify(params)
          : params,
      );
    }
    await insert(
      'produto',
      paramsToInsert,
      true,
    );
    return {
      status: 201,
      message: 'Produto cadastrado com sucesso',
    };
  } catch (error) {
    logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};
export const alterar = async (id, params) => {
  const paramsToChange = assembleParams(params, true);
  if (isLocal) {
    logger.info('ALTERAÇÃO DE PRODUTO:');
    logger.info(
      typeof params === 'object'
        ? JSON.stringify(params)
        : params,
    );
  }
  try {
    await update(
      'produto',
      paramsToChange,
    );
    return {
      status: 201,
      message: 'Produto alterado com sucesso',
    };
  } catch (error) {
    logger.error(error);
    return {
      status: 400,
      error,
    };
  }
};
export const excluir = async (id) => {};
