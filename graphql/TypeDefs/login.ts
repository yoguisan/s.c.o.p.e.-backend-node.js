const Login = `
input LoginInput {
  usuario: String!
  senha: String!
}

type Login {
  usuario: String!
  senha: String!
}
`;

export default Login;
