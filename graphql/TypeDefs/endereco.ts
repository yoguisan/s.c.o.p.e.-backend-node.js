const Endereco = `
input EnderecoInput {
  numero: Int!
  cidade: String!
  estado: String!
  bairro: String!
}

type Endereco {
  numero: Int!
  cidade: String!
  estado: String!
  bairro: String!
}
`;

export default Endereco;