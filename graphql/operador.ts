import IOperador from '@customTypes/operador';
import { signup } from '@services/operador';
import logger from '@utils/logger';
import EnderecoTypeDefs from './TypeDefs/endereco';
import LoginTypeDefs from './TypeDefs/login';

export const TypeDefs = `
  ${EnderecoTypeDefs}
  ${LoginTypeDefs}

  type Operador {
    id: Int!
    nome: String!
    usuario: String!
    endereco: Endereco
  }

  type Query {
    consultar(id: String!): Operador
    login(usuario: String!, senha: String!): Operador
  }

  type Mutation {
    cadastrar(
      nome: String!,
      endereco: EnderecoInput,
      login: LoginInput!,
     ): Boolean
  }
`;

export const resolvers = {
  Query: {

  },
  Mutation: {
    cadastrar: async (parent, args, context) => {
      const {
        endereco,
        nome,
        login,
      } = args;

      try {
        const response = await signup({
          nome,
          ...endereco,
          ...login,
        });

        console.log(response);

        return true;
      } catch (error) {
        logger.error(error);
        return false;
      }
    }
  }
};