import { ApolloServer, gql } from 'apollo-server-express';

import {
  TypeDefs as OperadorTypeDefs,
  resolvers as OperadorResolvers,
} from './operador';

const typeDefs = gql`
  ${OperadorTypeDefs}

  type Query {
    helloWorld: String!
  }

  type Mutation {
    createUser(name: String!): String!
  }
`;

const resolvers = {
  Query: {
    helloWorld: () => 'Hello World',
  },
  Mutation: {
    createUser: (parent, args, context) => JSON.stringify({ parent, args, context}),
  },
  ...OperadorResolvers,
};

const createServer = async (app) => {
  const Server = new ApolloServer({
    typeDefs,
    resolvers,
  });

  await Server.start()
    .then(() => Server.applyMiddleware({ app }));

  return Server;
}


export default createServer;
