
import { networkInterfaces } from 'os';

import CreateServer from '@graphql/server';
import '@controllers/campo';
import '@controllers/endereco';
import '@controllers/operador';
import '@controllers/produto';

import Config from './config';

const {
  app,
  PORT,
  ENV,
} = Config;

app.get('/', (req, res) => res.send({
  ok: true,
  message: 'Servidor rodando!',
}));

const interfaces = networkInterfaces();

const ip = interfaces[Object.keys(interfaces)[0]]
  .filter((device) => device.family === 'IPv4')[0];

app.listen(PORT, () => {
  switch (ENV.toLowerCase()) {
    case 'local':
      console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
      break;
    case 'dev':
      console.log(`⚡️[server]: Server is running at ${ip.address}:${PORT}`);
      break;
    default:
      break;
  }
});

CreateServer(app);
