import * as dotenv from 'dotenv';
import express, { Request, Response, Application } from 'express';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';

import { decrypt } from '@utils/crypto';

import specs, { SERVER_URL } from '@utils/swagger/config';

import morgan from '@utils/morgan';
import logger from '@utils/logger';

import { IConfig, IProcessEnv } from './types/config';

declare const process: {
  env: IProcessEnv;
}

dotenv.config();

const {
  ALGORITHM,
  CONSULTA_CEP_URL,
  MYSQL_DB,
  MYSQL_PASSWORD,
  MYSQL_URL,
  MYSQL_USER,
  NODE_ENV: ENV,
  LOG_PATH,
  PORT,
  REDIS_URL,
  SECRET_KEY,
} = process.env || {};

const {
  ACCESS_LOG_PATH,
  isLocal,
} = morgan;

const app: Application = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  '/api-docs',
  swaggerUi.serve,
  swaggerUi.setup(specs),
);

app.use(morgan);

app.use((req: Request,
  res: Response,
  next) => {
  const {
    method,
    body = {},
    query = {},
  } = req;

  switch (method) {
    case 'POST':
    case 'PUT':
      if (isLocal) {
        logger.info('REQUISIÇÃO INTERCEPTADA');
      }
      if (body?.iv && body?.content) {
        if (isLocal) {
          logger.info(`REQUISIÇÃO ${method} CRIPTOGRAFADA`);
          logger.info(decrypt(body));
        }
        req.body = decrypt(body);
      }
      break;
    case 'GET':
      if (query?.iv && query?.content) {
        if (isLocal) {
          logger.info('REQUISIÇÃO GET CRIPTOGRAFADA');
          logger.info(query);
        }
        const decrypted = JSON.parse(decrypt(query));
        req.query = decrypted;
      }
      break;
    default:
      if (isLocal) {
        logger.info(`REQUISIÇÃO ${method}`);
      }
      break;
  }
  next();
});

const Config: IConfig = {
  app,
  isLocal,
  ACCESS_LOG_PATH,
  ALGORITHM,
  CONSULTA_CEP_URL,
  ENV,
  LOG_PATH,
  MYSQL_URL,
  MYSQL_USER,
  MYSQL_PASSWORD,
  MYSQL_DB,
  PORT,
  REDIS_URL,
  SECRET_KEY,
  SERVER_URL,
};

export default Config;
