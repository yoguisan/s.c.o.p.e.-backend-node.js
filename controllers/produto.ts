import {
  validationResult,
  checkSchema,
  Schema,
} from 'express-validator';

import {
  alterar,
  buscar,
  cadastrar,
  excluir,
  listar,
} from '@services/produto';

import ProdutoSchema from '@utils/validationSchemas/produto';

const { cadastro: cadastroSchema } = ProdutoSchema;

import Config from '../config';

import Logger from 'utils/logger';

const { app } = Config;

// Cadastro
app.post(
  '/produto',
  checkSchema(cadastroSchema as Schema),
  async (req, res) => {
    const {
      originalUrl,
      body,
    } = req;

    const errors = validationResult(body);

    if (!errors.isEmpty()) {
      Logger.error({
        url: originalUrl,
        method: 'POST',
        errors: errors.array(),
      });
      return res.status(400).json({ errors: errors.array() });
    }
    const response = await cadastrar(typeof (body) !== 'object' ? JSON.parse(body) : body);
    if (!response) {
      return res.status(500).send('Sem resposta do servidor');
    }
    return res.status(response?.status || 200).send(response);
  },
);

// Listagem
app.get(
  '/produto/:idCampo',
  async(req, res) => {
    const {
      params: {
        idCampo,
      },
    } = req;

    if (!idCampo) {
      return res.status(400).json({
        errors: ['ID do campo não informado'],
      });
    }

    const response = await listar(idCampo, [
      'idProduto',
      'nome',
      'valor',
      'categoria',
      'idCampo',
    ]);
    /*
    if (isLocal) {
      logger.info('BUSCA DE DADOS DO CAMPO:');
      logger.info(response);
    }
    */
    return res.status(response.status || 200).send(response);
  },
);
