import {
  validationResult,
  checkSchema,
  Schema,
} from 'express-validator';

import {
  login, signup, buscarDados,
} from '@services/operador';

import OperadorSchema from '@utils/validationSchemas/operador';

const { login: loginSchema, signup: signupSchema } = OperadorSchema;

import Config from '../config';

const { app: App } = Config;

import Logger from '@utils/logger';

/**
 * @swagger
 * /login/operador:
 *   post:
 *     summary: Login de operador
*/
App.post(
  '/login/operador',
  checkSchema(loginSchema as Schema),
  async (req, res) => {
    const {
      originalUrl,
      body,
    } = req;

    const {
      usuario,
      senha,
    } = (typeof (body) !== 'object' ? JSON.parse(body) : body);

    const errors = validationResult(body);

    if (!errors.isEmpty()) {
      Logger.error({
        url: originalUrl,
        method: 'POST',
        errors: errors.array(),
      });
      return res.status(400).json({ errors: errors.array() });
    }
    const response = await login(usuario, senha);
    if (!response) {
      return res.status(500).send('Sem resposta do servidor');
    }
    return res.status(response?.status || 200).send(response);
  },
);

/**
 * @swagger
 * /operador:
 *   post:
 *     summary: Cadastro de operador
*/
App.post(
  '/operador',
  checkSchema(signupSchema as Schema),
  async (req, res) => {
    const {
      originalUrl,
      body,
    } = req;

    const errors = validationResult(body);

    if (!errors.isEmpty()) {
      Logger.error({
        url: originalUrl,
        method: 'POST',
        errors: errors.array(),
      });
      return res.status(400).json({ errors: errors.array() });
    }
    const response = await signup(typeof (body) !== 'object' ? JSON.parse(body) : body);
    if (!response) {
      return res.status(500).send('Sem resposta do servidor');
    }
    return res.status(response?.status || 200).send(response);
  },
);

/**
 * @swagger
 * /operador:
 *   get:
 *     summary: Busca de dados do operador
*/
App.get(
  '/operador',
  async (req, res) => {
    const {
      query,
      query: {
        id,
      },
    } = req;

    if (!id) {
      return res.status(400).send('ID do operador não informado');
    }
    const parametros = [];

    Object.keys(query).forEach((parametro) => {
      if (parametro !== 'id') {
        parametros.push(parametro);
      }
    });

    const response = await buscarDados(id, parametros);

    return res.status(response?.status || 200).send(response);
  },
);
