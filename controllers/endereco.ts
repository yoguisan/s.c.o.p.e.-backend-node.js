import {
  validationResult,
  checkSchema,
  Schema,
} from 'express-validator';

import EnderecoSchema from '@utils/validationSchemas/endereco';

const { cep: cepSchema } = EnderecoSchema;

import Config from '../config';

const {
  app,
  ENV,
  CONSULTA_CEP_URL: cepUrl,
} = Config;

import { get } from '@utils/services';

import logger from '@utils/logger';

const isLocal = ENV === 'local';

/**
 * @swagger
 * /cep:
 *   get:
 *     summary: Busca de CEP
*/
app.get(
  '/cep/:cep',
  checkSchema(cepSchema as Schema),
  async (req, res) => {
    const {
      params: {
        cep,
      },
    } = req;

    const errors = validationResult(cep);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const requestUrl = `${cepUrl}/${cep}/json`;

    if (isLocal) {
      logger.info('BUSCA DE CEP:');
      logger.info(requestUrl);
    }

    const result = await get(requestUrl);
    if (isLocal) {
      logger.info('RESULTADO DA BUSCA');
      logger.info(result);
    }

    return res.send(result);
  },
);
