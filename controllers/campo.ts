/* eslint-disable spaced-comment */
import {
  validationResult,
  checkSchema,
  Schema,
} from 'express-validator';

import Config from '../config';

import {
  login,
  signup,
  update,
  buscarDados,
} from '@services/campo';
import logger from '@utils/logger';

import CampoSchemas from '@utils/validationSchemas/campo';

const {
  login: loginSchema,
  signup: signupSchema,
  update: updateSchema,
} = CampoSchemas;

const { app, ENV } = Config;

const isLocal = ENV === 'local';

/**
 * @swagger
 * /login/campo:
 *   post:
 *     summary: Efetua o login do campo
*/
app.post(
  '/login/campo',
  checkSchema(loginSchema as Schema),
  async (req, res) => {
    const {
      originalUrl,
      body,
    } = req;

    const {
      usuario,
      senha,
    } = typeof (body) !== 'object' ? JSON.parse(body) : body;

    const errors = validationResult(body);

    if (!errors.isEmpty()) {
      logger.error({
        url: originalUrl,
        method: 'POST',
        errors: errors.array(),
      });
      return res.status(400).json({ errors: errors.array() });
    }
    const response = await login(usuario, senha);
    if (!response) {
      return res.status(500).send('Sem resposta do servidor');
    }
    return res.status(response?.status || 200)
      .send({
        ...response,
        status: response?.status || 200,
      });
  },
);

/**
 * @swagger
 * /campo:
 *   post:
 *     summary: Cadastra um novo campo
*/
app.post(
  '/campo',
  checkSchema(signupSchema as Schema),
  async (req, res) => {
    const {
      body: params,
    } = req;

    if (isLocal) {
      logger.info('REQUISIÇÃO PARA CADASTRAR CAMPO:');
      logger.info(req);
      logger.info(params);
    }

    const errors = validationResult(params);

    if (!errors.isEmpty()) {
      logger.error({
        url: '/campo',
        method: 'POST',
        errors: errors.array(),
      });
      return res.status(400).json({ errors: errors.array() });
    }

    const response = await signup(params);
    if (isLocal) {
      logger.info('CADASTRO DE NOVO CAMPO:');
      logger.info(response);
    }
    return res.status(response?.status || 200).send(response);
  },
);

/**
 * @swagger
 * /campo:
 *   put:
 *     summary: Altera os dados do campo
*/
app.put(
  '/campo',
  checkSchema(updateSchema as Schema),
  async (req, res) => {
    const {
      body: params,
    } = req;

    const errors = validationResult(params);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const response = await update(params);
    if (isLocal) {
      logger.info('ALTERAÇÃO DE DADOS DO CAMPO:');
      logger.info(response);
    }
    return res.status(response.status || 200).send(response);
  },
);

/**
 * @swagger
 * /campo:
 *   get:
 *     summary: Busca os dados do campo
*/
app.get(
  '/campo/:id',
  async (req, res) => {
    const {
      params: {
        id,
      },
    } = req;

    if (!id) {
      return res.status(400).json({
        errors: ['ID do campo não informado'],
      });
    }

    const response = await buscarDados(id, [
      'idCampo',
      'nome',
      'usuario',
      'endereco',
      'idDono',
      'verificado',
      'premium',
    ]);
    if (isLocal) {
      logger.info('BUSCA DE DADOS DO CAMPO:');
      logger.info(response);
    }
    return res.status(response.status || 200).send(response);
  },
);
